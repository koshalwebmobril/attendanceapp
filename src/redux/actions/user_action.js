import * as types  from '../type';
import Axios from 'axios';
import ApiUrl from '../../ApiUrl/ApiUrl';
import { Platform } from 'react-native';


export const submitLogin = (mobile, password) => dispatch => 
    new Promise((resolve ,reject) =>{

        dispatch({
            type: types.LOADING,
            isLoading:true
        });
        let formdata = new FormData();
        formdata.append("mobile",mobile);
        formdata.append("password",password);
       
     
       //*********************************** */
        Axios.post(ApiUrl.base_url+ApiUrl.login,formdata)
            .then(response => {

                dispatch({
                    type: types.LOADING,
                    isLoading:false
                });

               // console.log("hi1",response.data);

                if(response.data.status){
                 //   console.log("hi")

                    dispatch({
                        type:types.SAVE_USER_RESULTS,
                        user:response.data.data
                    })
                }
                resolve(response);

            })
            .catch(error => {

                dispatch({
                    type: types.LOADING,
                    isLoading:false
                });
                reject(error);
            })
    })


export const setLoading = (loading) => {

    return {
        type:types.LOADING,
        isLoading:loading
    }

}

export const setlogOutUser = (logOutUser) => {

    return {
        type:types.LogUser,
        log:logOutUser
    }

}

export const getAsyncStorage = (user) => {

    return {
        type:types.SAVE_USER_RESULTS,
        user:user
    }

}

export const onLogoutUser = () => {

    return {
        type:types.LOGOUT_USER,
        user:null
    }
}

export const saveUserResult = (user) => {

    return {
        type:types.SAVE_USER_RESULTS,
        user:user
    }
}


