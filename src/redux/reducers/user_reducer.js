import * as types  from '../type';

const initialState = {

    user_details:null

}


export default (state = initialState ,action) => {

    switch(action.type){

        case types.SAVE_USER_RESULTS :
            let user_details_action = action.user;
         

            if(user_details_action !== null){
                return{
                    ...state ,
                    user_details : {...user_details_action},
                }
            }else{
                return{
                    ...state ,
                    user_details : null,
                }
            }
        case types.LOGOUT_USER :
            return {
                ...state,
                user_details :action.user
            }

           default :
           return state;
    }




}


