

export default {

    // base_url : "http://webmobril.org/dev/attandanceapp/api/",
    base_url :"https://jordanrivervanlines.com/attandanceapp/api/",

    // slider_image_url: "http://webmobril.org/dev/questionnaire/public/upload/",
    slider_image_url: "https://jordanrivervanlines.com/attandanceapp/public/upload/",
    
    login : "login",

    post_attandance:"post-attandance",

    change_password: "change_password",

    forgot_password: "forgot-password",    

    contact_us: "contact_us",
     
     terms : "terms",
     
     privacy: "privacy",

     about_us:"about_us",
    
     jobs_history:"jobs-history",

     travel_times:"travel-times",

} 