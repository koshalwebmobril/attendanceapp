import React from 'react';
import { View, StyleSheet, Image,Text, TouchableOpacity, Platform,  } from 'react-native';
import { SafeAreaProvider, SafeAreaView } from 'react-native-safe-area-context';

const CustomHeader  = (props) => {

    return(
        <>
         {/* <SafeAreaView style={{backgroundColor:'#FFB91D'}}>
        <View style={styles.container}>
            <TouchableOpacity onPress={ props.nav} style={{ width:25,height:25,}} style={{position:'absolute',left:0}}>
                <Image source={props.img} style={styles.imageStyle} /> 
            </TouchableOpacity>
            <Text style={styles.headerText}>{props.title}</Text>

        </View>
        </SafeAreaView> */}
        </>
    )

}

const styles = StyleSheet.create({

    container:{
        backgroundColor:"#2C684F",
       // backgroundColor:"red",
        height: Platform.OS == 'ios' ? 40 : 50 ,
        width:"100%",
       justifyContent:"center",
        alignItems:'center',
        flexDirection:"row",
       
    },
    imageStyle:{
        width:20,
        height:20,
         alignSelf:'center',
        marginLeft:20
    },
    headerText:{
        fontSize:20,
        color:"white",
        alignSelf:'center'
    }
   
})

export default CustomHeader ;