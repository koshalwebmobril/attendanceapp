import React from 'react';
import { View, StyleSheet, Image,Text, TouchableOpacity, Platform,  } from 'react-native';
import { SafeAreaProvider, SafeAreaView } from 'react-native-safe-area-context';

const Custombutton  = (props) => {

    return(
      
        <View style={{backgroundColor:props.bgColor,width:'100%',height:50,borderRadius:5}}>
            <TouchableOpacity onPress={ props.press} style={{ width:'100%',height:50,justifyContent:'center'}}>
                 <Text style={{color:props.textColor,fontWeight:'500',alignSelf:'center',fontSize:18}}>{props.title}</Text>
            </TouchableOpacity>
        </View>
     
    )

}
export default Custombutton ;