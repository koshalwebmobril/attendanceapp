
import React,{useState, useRef,useEffect, memo} from 'react';
import {View ,FlatList, Text,Dimensions ,StyleSheet,Image,TouchableOpacity,StatusBar,TextInput,ActivityIndicator,ToastAndroid, Platform,Alert} from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

import AsyncStorage from '@react-native-community/async-storage';
import { useDispatch, useSelector } from 'react-redux';
import { SafeAreaView } from 'react-native-safe-area-context';
import Coustombutton from '../../Componant/Custombutton'
import { ScrollView } from 'react-native-gesture-handler';
import Toast from 'react-native-simple-toast';
import AwesomeAlert from 'react-native-awesome-alerts';
import DatePicker from 'react-native-datepicker';
import DateTimePickerModal from "react-native-modal-datetime-picker";
import { Container, Header, Content, Picker, Form } from "native-base";
import ApiUrl from '../../ApiUrl/ApiUrl';
// import { Dropdown } from 'react-native-material-dropdown';
import DropDownPicker from 'react-native-dropdown-picker';
import DateTimePicker from '@react-native-community/datetimepicker';
import * as types  from '../../redux/type';
// import CustomTable from 'react-native-table-component';
import { Table, TableWrapper, Row, Rows, Col, Cols, Cell } from 'react-native-table-component';

//import {submitLogin,saveUserResult} from/ '../../redux/actions/user_action';
var width = Dimensions.get('window').width
var height = Dimensions.get('window').height

const Mytimeseet = (props) => {
  
    const [tableData,settableData] = useState('');
    const [type,settype] = useState('');

    const dispatch = useDispatch();
    const loading = useSelector(state => state.common.loading);
    const user  = useSelector(state => state.user.user_details) 
    useEffect(() => {
        const unsubscribe = props.navigation.addListener('focus', () => {
        myList()
          });
        
          return () => {
            unsubscribe;
          };    }, [props.navigation]);

     // **************************************list********************************************

     const myList = async() => {
//  console.log('user Token is ',user.data.token)
          dispatch({
            type: types.LOADING,
            isLoading:true
            });

            AsyncStorage.getItem("selected").then(data =>{
              var typess= data=="Monthly" ? "Monthly" : "Week";
                    settype(typess);
                var attandance_type= data=="Monthly" ? 1 : 2;
                let formdata = new FormData();
                     fetch(ApiUrl.base_url+ApiUrl.jobs_history+'?attendance_type='+attandance_type, {
                     method: 'GET',
                     headers: {
                       'Content-Type': 'multipart/form-data',
                       'Accept':'application/json',
                       'Authorization': 'Bearer '+ user.data.token
                     },
                        // body: formdata
                   }).then((response) => response.json())
                         .then((responseJson) => {
                 //console.log('hi ashish kuamrverma ',responseJson)
                  if(responseJson.code==200){

                    if(attandance_type==1){

                      settableData(responseJson.data)

                    }else{
                    var data=responseJson.data;
                    var myTotalData=[]
                     //  *********************************************
                    var myArr=[]
                    var totalHour= 0

                    for(var i=0;i<data.week1.length;i++){
                      if(data.week1[i].times.length != 0){
                        for(var j=0;j<data.week1[i].times.length;j++){
                          myArr.push(data.week1[i].times[j])
                        }
                      }

                          if(i < data.week1.length){
                        var b = data.week1[i].total_time.split(':');
                        var totalminut= b[0]*60 + +b[1];
                         if(totalminut!=0){
                            totalHour=totalHour+totalminut
                         }                        
                        }
                     }

                 //    console.log('my array aftre for loo0ppp --->> ',myArr)

                   if(myArr.length != 0){
                    var h = totalHour/60;
                    var m = totalHour % 60;
                    var totatHoursMin= parseInt(h) + ':' + m
                      myTotalData.push({'week': myArr , 'totalHours':totatHoursMin ,'id':1})
                   }
                 //  console.log('my datta is 0000000',myTotalData)
                    //  *********************************************
                    var myArr2=[]
                    var totalHour2= 0
                     for(var i=0;i<data.week2.length;i++){
                      if(data.week2[i].times.length != 0){
                        for(var j=0;j<data.week2[i].times.length;j++){
                          myArr2.push(data.week2[i].times[j])
                        }
                      }
                      if(i < data.week2.length){
                        var b = data.week2[i].total_time.split(':');
                        var totalminut= b[0]*60 + +b[1];
                         if(totalminut!=0){
                            totalHour2=totalHour2+totalminut
                         }                        
                        }
                     }
                     if(myArr2.length != 0){
                      var h = totalHour2/60;
                      var m = totalHour2 % 60;
                      var totatHoursMin=parseInt(h) + ':' + m
                      myTotalData.push({'week': myArr2 , 'totalHours':totatHoursMin ,'id':2})
                   }
                //   console.log('my datta is 0000',myTotalData)
                  //  *********************************************
                     var myArr3=[]
                     var totalHour3= 0
                     for(var i=0;i<data.week3.length;i++){
                      if(data.week3[i].times.length != 0){
                        for(var j=0;j<data.week3[i].times.length;j++){
                          myArr3.push(data.week3[i].times[j])
                        }
                      }
                      if(i < data.week3.length){
                         var b = data.week3[i].total_time.split(':');
                         var totalminut= b[0]*60 + +b[1];
                         if(totalminut!=0){
                            totalHour3=totalHour3+totalminut
                         }                        
                        }
                     }
                     if(myArr3.length != 0){
                      var h = totalHour3/60
                      var m = totalHour3 % 60;
                      var totatHoursMin=parseInt(h) + ':' + m
                      myTotalData.push({'week': myArr3 , 'totalHours':totatHoursMin ,'id':3})
                   }
                //   console.log('my datta is 000',myTotalData)
                   //  *********************************************
                   var myArr4=[]
                   var totalHour4= 0
                   for(var i=0;i<data.week4.length;i++){
                    if(data.week4[i].times.length != 0){
                      for(var j=0;j<data.week4[i].times.length;j++){
                        myArr4.push(data.week4[i].times[j])
                      }
                    }
                    if(i < data.week4.length){
                      var b = data.week4[i].total_time.split(':');
                      var totalminut= b[0]*60 + +b[1];
                      if(totalminut!=0){
                         totalHour4=totalHour4+totalminut
                      }                        
                     }
                   }
                   if(myArr4.length != 0){
                    var h = totalHour4/60
                      var m = totalHour4 % 60;
                     
                      var totatHoursMin=parseInt(h) + ':' + m
                      myTotalData.push({'week': myArr4 , 'totalHours':totatHoursMin ,'id':4})
                    //myTotalData.push({'week': myArr4})
                 }
            //     console.log('my datta is 00 ',myTotalData)
                  //  *********************************************
                  var myArr5=[]
                  var totalHour5= 0
                  for(var i=0;i<data.week5.length;i++){
                    if(data.week5[i].times.length != 0){
                      for(var j=0;j<data.week5[i].times.length;j++){
                        myArr5.push(data.week5[i].times[j])
                      }
                    }
                    if(i < data.week5.length){
                      var b = data.week5[i].total_time.split(':');
                      var totalminut= b[0]*60 + +b[1];
                      if(totalminut!=0){
                         totalHour5=totalHour5+totalminut
                      }                        
                     }
                  }
                  if(myArr5.length != 0){
                    var h = totalHour5/60
                    var m = totalHour5 % 60;
                    var totatHoursMin=parseInt(h) + ':' + m
                      myTotalData.push({'week': myArr5 , 'totalHours':totatHoursMin ,'id':5})
                  }
                 // console.log('my datta is0 ',myTotalData)
                  //  *********************************************
                  var myArr6=[]
                  var totalHour6= 0
                  for(var i=0;i<data.week6.length;i++){
                    if(data.week6[i].times.length != 0){
                      for(var j=0;j<data.week6[i].times.length;j++){
                        myArr6.push(data.week6[i].times[j])
                      }
                    }
                    if(i < data.week6.length){
                      var b = data.week6[i].total_time.split(':');
                      var totalminut= b[0]*60 + +b[1];
                      if(totalminut!=0){
                         totalHour6=totalHour6+ +totalminut
                      }                        
                     }
                  }
                  if(myArr6.length != 0){
                      var h = totalHour6/60
                      var m = totalHour6 % 60;
                      var totatHoursMin=parseInt(h) + ':' + m
                      myTotalData.push({'week': myArr6 , 'totalHours':totatHoursMin ,'id':6})
                  
                  }
                  //  *********************************************

                 //    console.log('my datta is ',myTotalData)
                    settableData(myTotalData)

                }

                          dispatch({
                          type: types.LOADING,
                          isLoading:false
                                });
                        // Toast.show(responseJson.message);
                }else{
               //   console.log('hii')
                  dispatch({
                    type: types.LOADING,
                    isLoading:false
                });
                  Toast.show(responseJson.message);
                }
                         }).catch((error) => {
                     //      console.log('hiiiiiiiiiiii',error)
                            dispatch({
                                type: types.LOADING,
                                isLoading:false
                            });
                         });
             });
        }
 const  mydateCalculate = (items)=>{
  // var date = new Date(item);
  // date.setDate(date.getDate() + 7);
  // console.log('my calculated date is :- ',date);
//   var dates = [{
//     date: "01-01-2017 00:00:00",
//     dataField1: "",
//     dataField2: ""
//   },
//   {
//     date: "01-02-2017 00:00:00",
//     dataField1: "",
//     dataField2: ""
//   },
//   {
//     date: "01-15-2017 00:00:00",
//     dataField1: "",
//     dataField2: ""
//   },
//   {
//     date: "01-16-2017 00:00:00",
//     dataField1: "",
//     dataField2: ""
//   }
// ];

var group = Object.values(items.reduce((acc, val) => {

    var dArr = val.attandance_date.split("-");  // ex input "2010-01-18"
  // var adddd= dArr[2]+ "-" +dArr[1]+ "-" +dArr[0].substring(4)+"00:00:00"; //ex out: "18/01/10"

  //  var datesaa=val.attandance_date+"00:00:00";
  // var dateparts = val.date.split(/ |-|:/g);
  // var date = new Date(dateparts[2], dateparts[0] - 1, dateparts[1], dateparts[3], dateparts[4], dateparts[5]);
  // console.log('date formate is:--',date)
  var weekNo = getISO8601WeekNo(new Date(dArr[0],dArr[1],dArr[2],'00','00','00'));
  if (!acc[weekNo]) acc[weekNo] = [];
  acc[weekNo].push(val);
  return acc;
}, {}));

//console.log('the group data is ',group);

 }
function getISO8601WeekNo(date) {
  var startDate = new Date(date.getFullYear(), 0);
  var endDate = new Date(date.getFullYear(), date.getMonth(), date.getDate());
  while (endDate.getDay() < 6) endDate.setDate(endDate.getDate() + 1);
  endDate = endDate.getTime();
  var weekNo = 0;
  while (startDate.getTime() < endDate) {
    if (startDate.getDay() == 4) weekNo++;
    startDate.setDate(startDate.getDate() + 1);
  }
  return weekNo;
}

const AmPm = (date) => {
  var b = date.split(':');
  var hour= b[0];
  var minute= b[1];

  // var hour = date.toString().substring(16,18)
  // var minute = date.toString().substring(19,21)
   
     let am_pm = 'AM';
    
    if(hour>11){
      am_pm = 'PM';
      if(hour>12){
       var myhour = hour - 12;
       hour=myhour
      }
    }
    
    if(hour == 0){
      hour = 12;
    }
   if(hour.toString().length==1)
    {
       hour='0'+hour
    }
     const selectedTime = `${hour}:${minute} ${am_pm}` ;
     return selectedTime;
  };


const myFlatList=(myItems)=>{

 
  return(
  
    <FlatList
              showsHorizontalScrollIndicator={false}
              contentContainerStyle={{ alignItems: 'center'}}
              data={myItems}
              scrollEnabled={false}
              // horizontal={true}
              keyExtractor={item => item.id.toString()}
            //  keyExtractor={item => item.index_id.toString()}
              renderItem={({ item ,index}) => {
                return (
                    <>
                     
              <View style={{height:50,  flexDirection:'row',justifyContent:'space-between',backgroundColor:'white'}} >
               
                <TouchableOpacity style={{
                width:100,
                height:50,
                justifyContent:'center',
                borderBottomWidth:1,
                borderRightWidth:1,
                borderBottomColor:'gray',
                borderRightColor:'gray',}} onPress={()=>{props.navigation.navigate('Start',{item:item})}}>
                    <Image source={require('../../Assets/edit.png')} style={{width:25,height:25,alignSelf:'center'}}></Image>
                </TouchableOpacity>
              
                <View style={styles.viewStyle}>
                    <Text style={styles.textStyle}>{item.day}</Text>
                </View>
                <View style={styles.viewStyle}>
                    <Text style={styles.textStyle}>{item.attandance_date}</Text>
                </View>
                <View style={styles.viewStyle}>
                    <Text style={styles.textStyle}>{item.job_title}</Text>
                </View>
                <View style={styles.viewStyle}>
                    <Text style={styles.textStyle}>{AmPm(item.start_time)}</Text>
                </View>
                <View style={styles.viewStyle}>
                    <Text style={styles.textStyle}>{AmPm(item.end_time)}</Text>
                </View>
                <View style={styles.viewStyle}>
                    <Text style={styles.textStyle}>{item.travel_time}</Text>
                </View>
                <View style={styles.viewStyle}>
                    <Text style={styles.textStyle}>{item.total_time}</Text>
                </View>
                  </View> 
                  </>
                )
              }}
        /> 
    
  )
}



    return(
        <SafeAreaView style={{backgroundColor:'#2C2C2C',flex:1}}>
      <StatusBar backgroundColor='#2C2C2C'/>

      <View style={{flex:1,backgroundColor:'#FFB91D',height:'100%',width:'100%'}}>

      {/* ************************Header************************** */}
      <View style={{width:'100%',height:50,backgroundColor:'#2C2C2C' ,flexDirection:'row',alignItems:'center',paddingLeft:20,paddingRight:20}}>
       <TouchableOpacity onPress={()=>{props.navigation.navigate('Home')}}style={{height:15,width:25}}>
           <Image source={require('../../Assets/v1.png')} style={{height:20,width:11}}></Image>
       </TouchableOpacity>
      
           <Text style={{color:'white',fontSize:17,fontWeight:'600',top:3}}>My Timesheet</Text>

       </View>

      {/* ************************end Header************************** */}
   {  type!= "Monthly" ?
  
  <ScrollView showsVerticalScrollIndicator={false} horizontal={true}>
  <FlatList
           showsHorizontalScrollIndicator={false}
           contentContainerStyle={{ alignItems: 'center'}}
           data={tableData}
           // horizontal={true}
           keyExtractor={item => item.id.toString()}
           renderItem={({ item ,index}) => {
             return (
                 <>
                 
                   
                     <View style={{height:50,  flexDirection:'row',justifyContent:'space-between',backgroundColor:'white'}}>
                      <View style={{width:100,
                       height:50,
                       justifyContent:'center',
                       borderBottomWidth:1,
                       borderRightWidth:1,
                       borderBottomColor:'black',
                       borderRightColor:'black',
                       backgroundColor:'gray',}}>
                         <Text style={styles.textHeaderStyle}>{type + parseInt(index+1)}</Text>
                     </View>
                     <View style={styles.viewHeaderStyle}>
                         <Text style={styles.textHeaderStyle}>Day</Text>
                     </View>
                     <View style={styles.viewHeaderStyle}>
                         <Text style={styles.textHeaderStyle}>Date</Text>
                     </View>
                     <View style={styles.viewHeaderStyle}>
                         <Text style={styles.textHeaderStyle}>Title</Text>
                     </View>
                     <View style={styles.viewHeaderStyle}>
                         <Text style={styles.textHeaderStyle}>Start</Text>
                     </View>
                     <View style={styles.viewHeaderStyle}>
                         <Text style={styles.textHeaderStyle}>End</Text>
                     </View>
                     <View style={styles.viewHeaderStyle}>
                         <Text style={styles.textHeaderStyle}>Travel</Text>
                     </View>
                     <View style={styles.viewHeaderStyle}>
                         <Text style={styles.textHeaderStyle}>Total</Text>
                     </View>
                   
                   </View> 
                  
                  { myFlatList(item.week)}
                
                   
                  <View style={{height:50,  flexDirection:'row',justifyContent:'space-between',backgroundColor:'white'}}>
                   <View style={{width:100,
                    height:50,
                    justifyContent:'center',
                    borderBottomWidth:1,
                    borderRightWidth:1,
                    borderBottomColor:'black',
                    borderRightColor:'black',
                    backgroundColor:'white',}}>
                      <Text style={styles.textHeaderStyle}></Text>
                  </View>
                  <View style={styles.viewTotalStyle}>
                      <Text style={styles.textHeaderStyle}></Text>
                  </View>
                  <View style={styles.viewTotalStyle}>
                      <Text style={styles.textHeaderStyle}></Text>
                  </View>
                  <View style={styles.viewTotalStyle}>
                      <Text style={styles.textHeaderStyle}></Text>
                  </View>
                  <View style={styles.viewTotalStyle}>
                      <Text style={styles.textHeaderStyle}></Text>
                  </View>
                  <View style={styles.viewTotalStyle}>
                      <Text style={styles.textHeaderStyle}></Text>
                  </View>
                  <View style={styles.viewTotalStyle}>
                      <Text style={styles.textHeaderStyle}>Total Hours</Text>
                  </View>
                  <View style={styles.viewTotalStyle}>
                      <Text style={styles.textHeaderStyle}>{item.totalHours}</Text>
                  </View>
                
                </View>  
                   
                 

               </>
             )
           }}
     />


</ScrollView>
  :
  <ScrollView showsVerticalScrollIndicator={false} horizontal={true}>
     <FlatList
              showsHorizontalScrollIndicator={false}
              contentContainerStyle={{ alignItems: 'center'}}
              data={tableData}
              // horizontal={true}
            //   keyExtractor={item => item.id}
              renderItem={({ item ,index}) => {
                return (
                    <>
                    
                        {index==0 ? 
                        <View style={{height:50,  flexDirection:'row',justifyContent:'space-between',backgroundColor:'white'}}>
                         <View style={{width:100,
                          height:50,
                          justifyContent:'center',
                          borderBottomWidth:1,
                          borderRightWidth:1,
                          borderBottomColor:'black',
                          borderRightColor:'black',
                          backgroundColor:'gray',}}>
                            <Text style={styles.textHeaderStyle}>{type}</Text>
                        </View>
                        <View style={styles.viewHeaderStyle}>
                            <Text style={styles.textHeaderStyle}>Day</Text>
                        </View>
                        <View style={styles.viewHeaderStyle}>
                            <Text style={styles.textHeaderStyle}>Date</Text>
                        </View>
                        <View style={styles.viewHeaderStyle}>
                            <Text style={styles.textHeaderStyle}>Title</Text>
                        </View>
                        <View style={styles.viewHeaderStyle}>
                            <Text style={styles.textHeaderStyle}>Start</Text>
                        </View>
                        <View style={styles.viewHeaderStyle}>
                            <Text style={styles.textHeaderStyle}>End</Text>
                        </View>
                        <View style={styles.viewHeaderStyle}>
                            <Text style={styles.textHeaderStyle}>Travel</Text>
                        </View>
                        <View style={styles.viewHeaderStyle}>
                            <Text style={styles.textHeaderStyle}>Total</Text>
                        </View>
                      
            </View> 
                        : null} 


              <View style={{height:50,  flexDirection:'row',justifyContent:'space-between',backgroundColor:'white'}} >
               
                <TouchableOpacity style={{
                width:100,
                height:50,
                justifyContent:'center',
                borderBottomWidth:1,
                borderRightWidth:1,
                borderBottomColor:'gray',
                borderRightColor:'gray',}} onPress={()=>{props.navigation.navigate('Start',{item:item})}}>
                    <Image source={require('../../Assets/edit.png')} style={{width:25,height:25,alignSelf:'center'}}></Image>
                </TouchableOpacity>
              
                <View style={styles.viewStyle}>
                    <Text style={styles.textStyle}>{item.day}</Text>
                </View>
                <View style={styles.viewStyle}>
                    <Text style={styles.textStyle}>{item.attandance_date}</Text>
                </View>
                <View style={styles.viewStyle}>
                    <Text style={styles.textStyle}>{item.job_title}</Text>
                </View>
                <View style={styles.viewStyle}>
                    <Text style={styles.textStyle}>{AmPm(item.start_time)}</Text>
                </View>
                <View style={styles.viewStyle}>
                    <Text style={styles.textStyle}>{AmPm(item.end_time)}</Text>
                </View>
                <View style={styles.viewStyle}>
                    <Text style={styles.textStyle}>{item.travel_time}</Text>
                </View>
                <View style={styles.viewStyle}>
                    <Text style={styles.textStyle}>{item.total_time}</Text>
                </View>
                  </View> 
                  </>
                )
              }}
        />
 

 </ScrollView>


}
   
       </View>
       {
         loading  && (
          <View
          style={[
          StyleSheet.absoluteFill,
          { backgroundColor: 'rgba(0, 0, 0, 0.2)', justifyContent: 'center' }
          ]}
          >
              <ActivityIndicator color={"red"} size={40} />
      </View>
         )
       }

   </SafeAreaView>
    )
}

const styles = StyleSheet.create({

    container:{
       //flex:1,
        backgroundColor:"white",
        justifyContent:"space-between",
         alignItems:"center"
    },
    textHeaderStyle:{
        alignSelf:'center',
        color:'black',
        padding:10,
        fontWeight:'bold',
        fontSize:16
    },
  viewHeaderStyle:{
  justifyContent:'center',
  borderBottomWidth:1,
  borderRightWidth:1,
  borderBottomColor:'black',
  borderRightColor:'black',
  backgroundColor:'gray',
  width:120
  },
  viewTotalStyle:{
    justifyContent:'center',
    borderBottomWidth:1,
    borderRightWidth:1,
    borderBottomColor:'black',
    borderRightColor:'black',
    backgroundColor:'white',
    width:120
    },
  textStyle:{
    alignSelf:'center',
    color:'black',
    padding:10
},
viewStyle:{
justifyContent:'center',
borderBottomWidth:1,
borderRightWidth:1,
borderBottomColor:'gray',
borderRightColor:'gray',
width:120
},
})


export default memo(Mytimeseet) 