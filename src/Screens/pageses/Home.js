import React,{useState, useRef,useEffect} from 'react';
import {View , Text ,Dimensions,StyleSheet,Image,TouchableOpacity,StatusBar,TextInput,ActivityIndicator,ToastAndroid, Platform,Alert} from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

import AsyncStorage from '@react-native-community/async-storage';
import { useDispatch, useSelector } from 'react-redux';
import { SafeAreaView } from 'react-native-safe-area-context';
import Coustombutton from '../../Componant/Custombutton'
import { ScrollView } from 'react-native-gesture-handler';
import HTML from "react-native-render-html";
import * as types  from '../../redux/type';

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;
//import {submitLogin,saveUserResult} from/ '../../redux/actions/user_action';


const Home = (props) => {


    const [isSelect,setIsSelect] = useState(true);
    
    const [seleded,setSelected] = useState("");

    const [password,setPassword] = useState("");

    const [showAlert,setshowAlert] = useState(false);
   

    const dispatch = useDispatch();
    const loading = useSelector(state => state.common.loading);
    const user  = useSelector(state => state.user.user_details) 
    useEffect(() => {
        const unsubscribe = props.navigation.addListener('focus', () => {
           
            setPassword("")
        
          });
        
          return () => {
            unsubscribe;
          };
    }, [props.navigation]);

    const logoutPressed = () => {
    
   setshowAlert(false)
  AsyncStorage.clear();
   dispatch({
    type:types.SAVE_USER_RESULTS,
    user:null
  })
    }

    const select = (item) => {
    
        setIsSelect(false)
        setSelected(item)
        AsyncStorage.setItem("selected",item);
         }

    if(loading){
        return (<View
                    style={[
                    StyleSheet.absoluteFill,
                    { backgroundColor: 'rgba(0, 0, 0, 0.2)', justifyContent: 'center' }
                    ]}
                    >
                        <ActivityIndicator color={"#2C2C2C"} size={30} />
                </View>)
      }

    return(
        <>
        <SafeAreaView style={{backgroundColor:'#2C2C2C'}}/>
        <StatusBar backgroundColor='#2C2C2C'/>
        <View style={{flex:1,backgroundColor:'#FFB91D',height:'100%',width:'100%'}}>

{/* ************************Header************************** */}
       <View style={{width:'100%',height:50,backgroundColor:'#2C2C2C' ,justifyContent:'space-between',flexDirection:'row',alignItems:'center',paddingLeft:20,paddingRight:20}}>
       <View>
           <Text style={{color:'white',fontSize:17,fontWeight:'700'}}>E Attendance</Text>
       </View>

       <TouchableOpacity onPress={()=>{setshowAlert(!showAlert)}}>
           <Text style={{color:'white',fontSize:15,fontWeight:'500'}}>Logout</Text>
       </TouchableOpacity>
       </View>

{/* ************************end Header************************** */}
               <View style={{width:'100%',height:windowHeight*29/100,backgroundColor:'white',marginTop:2}}>
               <Image  source={{
                   uri: user.slider_image,
        }}  style={{height:'100%',width:'100%',resizeMode:'stretch'}}/>
                </View>

      <ScrollView style={{padding:15}}>

                {/* <Text style={{fontSize:16,fontWeight:'500',color:'black'}}>About</Text>
                <View style={{width:10,height:13}} /> */}

        {/* <Text style={{color:'#2C2C2C',fontSize:11,lineHeight: 15,}}>{user.about_us}</Text>  #494949*/}



                <HTML
                 html={user.about_us}
                 imagesMaxWidth={Dimensions.get("window").width}
                 />


      
      <View style={{width:windowWidth,height:50}}></View>
       </ScrollView>
      <View style={{width:'95%',alignSelf:'center',marginBottom:30,flexDirection:'row',justifyContent:'space-between'}}>
        <View style={{width:'47%'}}>
                  <Coustombutton bgColor={'white'} press={()=>{props.navigation.navigate('Start',{item:null})}} textColor={'#2C2C2C'} title={'Log Attendance'}/>
        </View>
        <View style={{width:'47%'}}>
                  <Coustombutton bgColor={'white'} press={()=>{props.navigation.navigate('Mytimeseet')}} textColor={'#2C2C2C'} title={'My Timesheet'}/>
        </View>
      </View>  

{/* **********************************************logout Alert***************************************** */}


{showAlert ?
<View style={{height:windowHeight,width:windowWidth,position:'absolute',backgroundColor:'rgba(0,0,0,0.7)'}}>
        {/* <SafeAreaView style={{backgroundColor:'#2C2C2C'}}/>
        <StatusBar backgroundColor='#2C2C2C'/> */}
    <View style={{backgroundColor:'white',alignSelf:"center",marginTop:'73%',height:180,width:250,padding:20,borderRadius:8}}>
    <Text style={{fontSize:17,fontWeight:'500',alignSelf:'center',color:'#494949'}}>Are you sure you want</Text>
    <Text style={{fontSize:17,fontWeight:'500',alignSelf:'center',color:'#494949'}}>to logout</Text>
    
    <View style={{flexDirection:'row',width:200,alignSelf:'center',marginTop:20,paddingLeft:10}}>
     <TouchableOpacity style={{height:30,width:70,backgroundColor:'#FFB91D',justifyContent:'center',borderRadius:5,margin:10}} onPress={()=>{logoutPressed()}}>
       <Text style={{alignSelf:'center'}}>Yes</Text>
     </TouchableOpacity>

     <TouchableOpacity style={{height:30,width:70,backgroundColor:'#EAEAEA',justifyContent:'center',borderRadius:5,margin:10}} onPress={()=>{setshowAlert(false)}}>
       <Text style={{alignSelf:'center'}}>No</Text>
     </TouchableOpacity>

    </View>
</View>
</View>

:
null
}



{/* *********************************************selection**************************************** */}


{isSelect ?
<View style={{height:windowHeight,width:windowWidth,position:'absolute',backgroundColor:'rgba(0,0,0,0.7)'}}>
        {/* <SafeAreaView style={{backgroundColor:'#000'}}/>
        <StatusBar backgroundColor='#2C2C2C'/> */}
    <View style={{backgroundColor:'white',alignSelf:"center",marginTop:'73%',height:145,width:230,padding:20,borderRadius:8}}>
    <Text style={{fontSize:18,fontWeight:'500',alignSelf:'center',color:'#494949'}}>Please Select</Text>
    
    <View style={{width:150,alignSelf:'center',marginTop:5,paddingLeft:15}}>
     <TouchableOpacity style={{height:30,width:100,backgroundColor:'#FFB91D',justifyContent:'center',borderRadius:5,margin:10}} onPress={()=>{select('Monthly')}}>
       <Text style={{alignSelf:'center'}}>Monthly</Text>
     </TouchableOpacity>

     <TouchableOpacity style={{height:30,width:100,backgroundColor:'#EAEAEA',justifyContent:'center',borderRadius:5,marginLeft:10}} onPress={()=>{select('Weekly')}}>
       <Text style={{alignSelf:'center'}}>Weekly</Text>
     </TouchableOpacity>

    </View>
</View>
</View>

:
null
}



       </View>
        </>
    )
}


export default Home;