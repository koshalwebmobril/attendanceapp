
import React,{useState, useRef,useEffect} from 'react';
import {View ,FlatList, Text,Dimensions ,StyleSheet,Image,TouchableOpacity,StatusBar,TextInput,ActivityIndicator,ToastAndroid, Platform,Alert} from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

import AsyncStorage from '@react-native-community/async-storage';
import { useDispatch, useSelector } from 'react-redux';
import { SafeAreaView } from 'react-native-safe-area-context';
import Coustombutton from '../../Componant/Custombutton'
import { ScrollView } from 'react-native-gesture-handler';
import Toast from 'react-native-simple-toast';
import AwesomeAlert from 'react-native-awesome-alerts';
import DatePicker from 'react-native-datepicker';
import DateTimePickerModal from "react-native-modal-datetime-picker";
import { Container, Header, Content, Picker, Form } from "native-base";
import ApiUrl from '../../ApiUrl/ApiUrl';
// import { Dropdown } from 'react-native-material-dropdown';
import DropDownPicker from 'react-native-dropdown-picker';
import DateTimePicker from '@react-native-community/datetimepicker';
import * as types  from '../../redux/type';

//import {submitLogin,saveUserResult} from/ '../../redux/actions/user_action';
var width = Dimensions.get('window').width
var height = Dimensions.get('window').height

const Start = (props) => {
  

    const [date,setDate] = useState('');
    const [enddate,setEndDate] = useState('');
    const [dropDownTitle,setdropDownTitle] = useState([]);
    const [dropDownTravelTime,setdropDownTravelTime] = useState([]);
    const [jobs,setJobs] = useState("");
   
    const [jobTitle,setjobTitle] = useState();
   
    const [saveStatus,setsaveStatus] = useState(false);
    const [attendancetype,setattendancetype] = useState('');

    const [pickerNo,setpickerNo] = useState("");

    let controller
    const user  = useSelector(state => state.user.user_details) 
    const [isDatePickerVisible, setDatePickerVisibility] = useState(false);
 
   
    const [startTime,setStartTime] = useState("");
    const [myStartTime,setMyStartTime] = useState("");
   

    const [endTime,setEndTime] = useState("");
    const [myEndTime,setMyEndTime] = useState("");
    const [id,setIds] = useState("");
   

    const [travelTime,setTravelTime] = useState("");
  
    
    const [totalTime,setTotalTime] = useState("");
    
    const [duration,setduration] = useState("");

    const dispatch = useDispatch();
    const loading = useSelector(state => state.common.loading);

    useEffect(() => {
      // dispatch({
      //   type: types.LOADING,
      //   isLoading:false
      //   });
        const unsubscribe = props.navigation.addListener('focus', () => {
          myDropDownList();
           if(props.route.params.item!=null){
          //   console.log('item is :- ',props.route.params.item)
            setDate(props.route.params.item.attandance_date)
            setEndDate(props.route.params.item.end_date)
            setattendancetype(props.route.params.item.attendance_type)
            setJobs(props.route.params.item.job_id)
            setjobTitle(props.route.params.item.job_title)
            setStartTime(props.route.params.item.start_time.substring(0,5))
            ST_AM_PM(props.route.params.item.start_time)
            setduration("00")
            setMyStartTime(props.route.params.item.start_time)  
            setEndTime(props.route.params.item.end_time.substring(0,5))
            ET_AM_PM(props.route.params.item.end_time)
            setMyEndTime(props.route.params.item.end_time)
           if(props.route.params.item.job_title=='Warehouse'){
             setTravelTime(null)
           }else if(props.route.params.item.travel_time.substring(0,5)=='00:00'){
            setTravelTime(null)
           }else{
            setTravelTime(props.route.params.item.travel_time.substring(0,5))
           }
            
            setTotalTime(props.route.params.item.total_time)
          
            setIds(props.route.params.item.id)
           }else{
            AsyncStorage.getItem("selected").then(data =>{
            setattendancetype(data=="Monthly" ? 1 : 2)
            setDate("")
            setEndDate("")
            setJobs("")
            // setjobTitle("")
            setStartTime("")
            setEndTime("")
            setduration("00")
            // setTravelTime("")
            setTotalTime("")
          
            setIds("")
          })
           }

         
          });
        
          return () => {
            unsubscribe;
          };    }, [props.navigation]);



           const myDropDownList=()=>{
              dispatch({
                type: types.LOADING,
                isLoading:true
                });

                    var formdata = new FormData();
                         fetch(ApiUrl.base_url+ApiUrl.travel_times, {
                         method: 'GET',
                         headers: {
                           'Content-Type': 'multipart/form-data',
                           'Accept':'application/json',
                           'Authorization': 'Bearer '+ user.data.token
                         },
                            // body: formdata
                       }).then((response) => response.json())
                             .then((responseJson) => {
                 //    console.log('hi ashish kuamrverma ',responseJson)
                      if(responseJson.code==200){
    
                        setdropDownTitle(responseJson.job_titles)
                        setdropDownTravelTime(responseJson.travel_times)
                        //  console.log('my datta is ',myTotalData)
                    
                              dispatch({
                              type: types.LOADING,
                              isLoading:false
                                    });
          
                    }else{
                    //  console.log('hii')
                      dispatch({
                        type: types.LOADING,
                        isLoading:false
                    });
                      Toast.show(responseJson.message);
                    }
                             }).catch((error) => {
                            //   console.log('hiiiiiiiiiiii',error)
                                dispatch({
                                    type: types.LOADING,
                                    isLoading:false
                                });
                             });
                 
}

    const submit = () => {
      if(saveStatus==true){
        AsyncStorage.getItem("selected").then(data =>{
          setattendancetype(data=="Monthly" ? 1 : 2)
      setDate("")
      setJobs("")
      setEndDate("")
      setjobTitle(null)
      setTravelTime(null)
      setStartTime("")
      setEndTime("")
      setTotalTime("")
      setMyStartTime("")
      setMyEndTime("")
      setduration("00")
      setIds("")
      setsaveStatus(false)
    
   
     Toast.show('Your job is saved please fill other jobs');
        });
      }else{
        Toast.show('Please fill the above details and save');
      }
    }

  const ST_AM_PM = (date)=>{
    var hour = date.toString().substring(0,2)
    var minute = date.toString().substring(3,5)
        let am_pm = 'AM'; 
        if(hour>11){
          am_pm = 'PM';
          if(hour>12){
            hour = hour - 12;
          }
        }
        if(hour == 0){
          hour = 12;
        }
        if(hour.toString().length==1)
        {
           hour='0'+hour
        }
         const selectedTime = `${hour}:${minute} ${am_pm}` ;
         setStartTime(selectedTime)
  }
  const ET_AM_PM = (date)=>{
    var hour = date.toString().substring(0,2)
    var minute = date.toString().substring(3,5)
  //  console.log('fghj',hour," gdhjzj",minute)
        let am_pm = 'AM'; 
        if(hour>11){
          am_pm = 'PM';
          if(hour>12){
            hour = hour - 12;
          }
        }
        if(hour == 0){
          hour = 12;
        }
        if(hour.toString().length==1)
        {
           hour='0'+hour
        }
         const selectedTime = `${hour}:${minute} ${am_pm}` ;
         setEndTime(selectedTime)
  }


    const showDatePicker = () => {
      setDatePickerVisibility(true);
    };
  
    const hideDatePicker = () => {
      setDatePickerVisibility(false);
    };
  
    const handleConfirmStartTime = (dates) => {
      console.warn("A time has been picked:",dates.toString());
    var time = dates.toString().substring(16,21)
    var hour = dates.toString().substring(16,18)
    var minute = dates.toString().substring(19,21)
    // console.warn("A HH:",hour);
    // console.warn("A MM:",minute);
    // console.warn("A time has been picked:",time);
   
      setMyStartTime(dates.toString().substring(16,24))
      hideDatePicker();
      dateCalculate(jobTitle,date,dates.toString().substring(16,24),enddate,myEndTime,travelTime); 

        let am_pm = 'AM';
        
        if(hour>11){
          am_pm = 'PM';
          if(hour>12){
            hour = hour - 12;
          }
        }
        
        if(hour == 0){
          hour = 12;
        }
        if(hour.toString().length==1)
        {
           hour='0'+hour
        }
         const selectedTime = `${hour}:${minute} ${am_pm}` ;
         setStartTime(selectedTime)
         console.warn("A final time is :",selectedTime);
    };

    const handleConfirmEndTime = (dates) => {
      var time = dates.toString().substring(16,21)
      var hour = dates.toString().substring(16,18)
      var minute = dates.toString().substring(19,21)
       //  console.log("A time has been picked: ", time);
        // console.log("2020-12-20T06:54:45.240Z: ", dates);
       //  console.log("check formate ", dates.toString());
        
        setMyEndTime(dates.toString().substring(16,24));
        hideDatePicker();

        dateCalculate(jobTitle,date,myStartTime,enddate,dates.toString().substring(16,24),travelTime);

        let am_pm = 'AM';
        
        if(hour>11){
          am_pm = 'PM';
          if(hour>12){
           var myhour = hour - 12;
           hour=myhour
          }
        }
        
        if(hour == 0){
          hour = 12;
        }
       if(hour.toString().length==1)
        {
           hour='0'+hour
        }
         const selectedTime = `${hour}:${minute} ${am_pm}` ;
         setEndTime(selectedTime)
      };

 
// ***********************date calculation******************************** 
  const msToTime = (duration) =>{
                var milliseconds = parseInt((duration % 1000) / 100),
                seconds = Math.floor((duration / 1000) % 60),
                minutes = Math.floor((duration / (1000 * 60)) % 60),
                hours = Math.floor((duration / (1000 * 60 * 60)));
            
              hours = (hours < 10) ? "0" + hours : hours;
              minutes = (minutes < 10) ? "0" + minutes : minutes;
              seconds = (seconds < 10) ? "0" + seconds : seconds;
              // console.log('//minute',minutes)
              // console.log('//hour',hours)
             
              if(hours.length>2 || hours=='NaN')
              {
                setduration(duration)
                setTotalTime('NaN')
              }
              else if(duration > 86400000){
                setduration(duration)
                setTotalTime(hours + ":" + minutes)
              }
                else{
                 setduration(duration)
                 setTotalTime(hours + ":" + minutes) // + ":" + seconds + "." + milliseconds;
                   }
             }
           
 // ***********************date calculation********************************
  const dateCalculate=(title,start_date,start_time,end_date,end_time,traveldate)=>{
    // console.log('***********************************')
    // console.log('title',title)
    // console.log('startdate',start_date)
    // console.log('starttime',start_time)
    // console.log('enddate',end_date)
    // console.log('endtime',end_time)
    // console.log('traveltime',traveldate)

    var travel=traveldate;
    var temp_end_Date=end_date;
    if(title =="Warehouse")
    {
      travel='00:00';
       //setTravelTime("")
    }
    if(traveldate==null){
      travel='00:00';
      // setTravelTime("")
    }

    if(start_time!="" && end_time !="" && start_date!='' && end_date!='')
    {
      const hh=travel.substring(0,2)
      const mm=travel.substring(3,5) 
      const travel_hours=hh*3600000
      const travel_minut=mm*60000

      
         const diffrentTime= new Date(end_date + 'T'+end_time)-new Date(start_date +'T'+ start_time);
         
           var totalDiffrentTime=0
         if(diffrentTime<0)
         {
          totalDiffrentTime= totalDiffrentTime + diffrentTime
         }
         else{
          totalDiffrentTime= totalDiffrentTime + diffrentTime+travel_hours+travel_minut
             }
          
             msToTime(totalDiffrentTime)
    }else{ 
    
          
    }
 
  }

// **************************************validation********************************************
 
      const valid=()=>{
        // if(date==""){
        //   Toast.show('Please select date');
        //   return false
        // }else 
        if(jobs==""){
          Toast.show('Please enter job#');
          return false
        }else if((/^\s*$/g.test(jobs))){
          Toast.show('Please enter job#');
          return false
        }else if(jobTitle=="" || jobTitle==null){
          Toast.show('Please select job title');
          return false
        }else if(date==""){
          Toast.show('Please select start date');
          return false
        }else if(startTime==""){
          Toast.show('Please enter start time');
          return false
        }else if(enddate==""){
          Toast.show('Please select end date');
          return false
        }else if(endTime==""){
          Toast.show('Please enter end time');
          return false
        }else if(endTime==startTime){
          Toast.show('Start and end time should not be same');
          return false
        }else if(duration > 86400000){
          Toast.show('Please put valid time.Total time is exceeding your day limit.');
          return false
        }else if(totalTime=="NaN"){
          Toast.show('Please enter valid start and end time');
          return false
        }else if(new Date(enddate)- new Date(date) < 0 ){
        
          Toast.show('Please enter valid start and end date');
          return false
        }else {
         
          return true
        }
      }

     // **************************************validation********************************************

  const mySave = async() => {
    if(valid()){
     
     // console.log('ashish id is ====',id)

      dispatch({
        type: types.LOADING,
        isLoading:true
        });

     var travel_times=travelTime

        if(travelTime=="" || travelTime==null || jobTitle=='Warehouse')
        {
           travel_times="00:00"
          //  setTravelTime("00:00")
        }
              let formdata = new FormData();
              if(id!="" || id != null){
                formdata.append("id",id);
              }
              formdata.append("date",date); 
              formdata.append("end_date",enddate); 
              formdata.append("attendance_type",attendancetype); 
              formdata.append("job_id",jobs);
              formdata.append("job_title",jobTitle);
              formdata.append("start_time",myStartTime.substring(0,5));
              formdata.append("end_time",myEndTime.substring(0,5));
              formdata.append("travel_time",travel_times);
              formdata.append("total_time",totalTime);
              
             // console.log('ashish form data is ====',formdata)
                 fetch(ApiUrl.base_url+ApiUrl.post_attandance, {
                 method: 'POST',
                 headers: {
                   'Content-Type': 'multipart/form-data',
                   'Accept':'application/json',
                   'Authorization': 'Bearer '+ user.data.token
                 },
                body: formdata
               }).then((response) => response.json())
                     .then((responseJson) => {
                      
            //  console.log('SignUp-->>',responseJson)
              if(responseJson.code==200){
                setIds(responseJson.job_id)
                setsaveStatus(true)
                dispatch({
                  type: types.LOADING,
                  isLoading:false
              });
               Toast.show(responseJson.message);   
                
            }else{
           //   console.log('hii')

              dispatch({
                type: types.LOADING,
                isLoading:false
            });
              Toast.show(responseJson.message);
            }
            
                     }).catch((error) => {
                   //    console.log('hiiiiiiiiiiii',error)
                        dispatch({
                            type: types.LOADING,
                            isLoading:false
                        });
                       
                     });
          }

    }




    return(
        <SafeAreaView style={{backgroundColor:'#2C2C2C',flex:1}}>
      <StatusBar backgroundColor='#2C2C2C'/>
{/* {console.log('my data is ======>>>',dropDownTitle)} */}
      <View style={{flex:1,backgroundColor:'#FFB91D',height:'100%',width:'100%'}}>

      {/* ************************Header************************** */}
      <View style={{width:'100%',height:50,backgroundColor:'#2C2C2C' ,flexDirection:'row',alignItems:'center',paddingLeft:20,paddingRight:20}}>
       <TouchableOpacity onPress={()=>{props.navigation.goBack()}}style={{height:15,width:25}}>
           <Image source={require('../../Assets/v1.png')} style={{height:20,width:11}}></Image>
       </TouchableOpacity>
      
           <Text style={{color:'white',fontSize:17,fontWeight:'600',top:3}}>E Attendance</Text>

       </View>

      {/* ************************end Header************************** */}
      <ScrollView
      alwaysBounceVertical={false}
      >
       <KeyboardAwareScrollView 
       automaticallyAdjustContentInsets={ true }
       contentContainerStyle={{ backgroundColor:"#FFB91D",padding:10,flexGrow:1,alignItems:"center"}}
       >
    


      
  
    <View style={{borderRadius:5,width:width*90/100,backgroundColor:'white',marginTop:20}}>
 {/* <View style={{width:width*90/100,height:20,backgroundColor:'#FFB91D'}} /> */}

 <View style={{borderColor:'black',borderWidth:0.5,borderRadius:5}}>
 

{/* **********************************  job#  ********************************************* */}
<View style={{width:width*90/100,flexDirection:'row',borderRadius:5,borderBottomColor:'black',borderBottomWidth:0.4}}>
<View style={{width:width*45/100,height:50,borderRightColor:'black',borderRightWidth:0.5,justifyContent:'center'}}>
   <Text style={{marginLeft:'15%',color:'#2C2C2C'}}>Job#</Text>
     </View>

      <View style={{width:width*45/100,height:50}}>
                   <TextInput 
                    onChangeText={(value)=>{
                      setJobs(value)
                      // checkJobError(value)
                    }}
                    value={jobs}
                    placeholder="Enter job"
                    keyboardType={Platform.OS === 'ios' ? 'ascii-capable' : 'visible-password'}
                    maxLength={25}
                    placeholderTextColor="black"
                    style={{width:'98%',height:45,paddingLeft:'10%'}}
                    />
                  
     </View>
 </View>
{/* *********************************Job Title**************************************** */}
<View style={{width:width*90/100,flexDirection:'row',borderRadius:5,borderBottomColor:'black',borderBottomWidth:0.4,zIndex:1000}}>
<View style={{width:width*45/100,height:50,borderRightColor:'black',borderRightWidth:0.5,justifyContent:'center'}}>
   <Text style={{marginLeft:'15%',color:'#2C2C2C'}}>Job Title</Text>
     </View>

      <View style={{width:width*45/100,height:50,zIndex:1000}}>
  
    <Picker
        
              placeholder={"Select"}
              selectedValue={jobTitle}
              style={{height: 40, width: '95%', left:'6%'}}
              onValueChange={(itemValue, itemIndex) =>{
                  setjobTitle(itemValue)
                  dateCalculate(itemValue,date,myStartTime,enddate,myEndTime,travelTime);
              }}
            >
          {/* <Picker.Item label="Select" value="" /> */}
          {dropDownTitle.map((item, index) => {
                return <Picker.Item key={index} label={item.label} value={item.value} />
            })}
        </Picker>
   
   {/* }  */}

     </View>
 </View>
{/* *****************************Start date******************************************** */}

 <View style={{width:width*90/100,flexDirection:'row',borderRadius:5,borderBottomColor:'black',borderBottomWidth:0.4}}>
     <View style={{width:width*45/100,height:50,borderRightColor:'black',borderRightWidth:0.5,justifyContent:'center'}}>
   <Text style={{marginLeft:'15%',color:'#2C2C2C'}}>Start Date</Text>
     </View>

      <View style={{width:width*45/100,height:50,}}>
      <View style={{flexDirection: 'row',top:5}}>
                    <DatePicker
                      customStyles={{
                        dateInput: {borderColor:'transparent'},
                        dateText: {color:'black'},
                        dateIcon: styles.dateIcon,
                        dateplaceholder: {
                          alignContent: 'flex-start',
                        },
                        placeholderText: {
                          fontSize: 15,
                          fontFamily: Platform.OS == 'android'
                            ? 'Calisto-Regular'
                            : 'System',
                          color: '#47170d',
                          marginLeft: '5%',
                        },
                      }}
                     
                      androidMode={'spinner'}
                      readOnly={true}
                      style={[styles.datePickerSelectInput]}
                      date={date}
                      mode="date"
                      placeholder={'Select date'}
                      maxDate={new Date ()}
                      format='YYYY-MM-DD'
                      confirmBtnText="Confirm"
                      cancelBtnText="Cancel"
                      iconSource={require ('../../Assets/calendra.png')}
                      onDateChange={date => {
                        setDate(date)
                        dateCalculate(jobTitle,date,myStartTime,enddate,myEndTime,travelTime);
                       // startAndEndDateCalculation(jobTitle,date,myStartTime,enddate,myEndTime,travelTime)
                      }}
                    />
                 
                  </View>

     </View>
 </View>

{/* *****************************Start Time******************************************** */}
<View style={{width:width*90/100,flexDirection:'row',borderRadius:5,borderBottomColor:'black',borderBottomWidth:0.4,zIndex:-10000}}>
<View style={{width:width*45/100,height:50,borderRightColor:'black',borderRightWidth:0.5,justifyContent:'center',zIndex:-100}}>
   <Text style={{marginLeft:'15%',color:'#2C2C2C'}}>Start Time</Text>
     </View>
    
     <TouchableOpacity 
    onPress={()=>{ 
        showDatePicker()
        setpickerNo("startTime")
      }}

      style={{width:width*45/100,height:50,zIndex:-1000000}}>

        { pickerNo=="startTime" ? 
         <DateTimePickerModal                         //line 54  set default time in ios 2020-12-20T00:00:00
              value={startTime}                       // line 76 set default time in android 2020-12-20T18:30:00
              isVisible={isDatePickerVisible}
              mode="time" 
              // locale='en_GB'
              is24Hour={false}
              timePickerModeAndroid="spinner"
              display="spinner"
              onConfirm={handleConfirmStartTime}
              onCancel={hideDatePicker}
            />
      
            :
            null
            }
      <View style={{top:10,zIndex:-10000}}>

       
         <Text style={{left:'8%'}}> {
          startTime.length > 0
          ? startTime
          : '__:__'
         
        }</Text>
                 
         </View>
    
     </TouchableOpacity>
 </View>
{/* *****************************End Date******************************************** */}

 <View style={{width:width*90/100,flexDirection:'row',borderRadius:5,borderBottomColor:'black',borderBottomWidth:0.4}}>
     <View style={{width:width*45/100,height:50,borderRightColor:'black',borderRightWidth:0.5,justifyContent:'center'}}>
   <Text style={{marginLeft:'15%',color:'#2C2C2C'}}>End Date</Text>
     </View>

      <View style={{width:width*45/100,height:50,}}>
      <View style={{flexDirection: 'row',top:5}}>
                    <DatePicker
                      customStyles={{
                        dateInput: {borderColor:'transparent'},
                        dateText: {color:'black'},
                        dateIcon: styles.dateIcon,
                        dateplaceholder: {
                          alignContent: 'flex-start',
                        },
                        placeholderText: {
                          fontSize: 15,
                          fontFamily: Platform.OS == 'android'
                            ? 'Calisto-Regular'
                            : 'System',
                          color: '#47170d',
                          marginLeft: '5%',
                        },
                      }}
                     
                      androidMode={'spinner'}
                      readOnly={true}
                      style={[styles.datePickerSelectInput]}
                      date={enddate}
                      mode="date"
                      placeholder={'Select date'}
                      maxDate={new Date ()}
                      format='YYYY-MM-DD'
                      confirmBtnText="Confirm"
                      cancelBtnText="Cancel"
                      iconSource={require ('../../Assets/calendra.png')}
                      onDateChange={end_date => {
                        setEndDate(end_date)
                        dateCalculate(jobTitle,date,myStartTime,end_date,myEndTime,travelTime);
                       // startAndEndDateCalculation(jobTitle,date,myStartTime,end_date,myEndTime,travelTime)
                      }}
                    />
                 
                  </View>

     </View>
 </View>

{/* **********************************End Time************************************* */}
<View style={{width:width*90/100,flexDirection:'row',borderRadius:5,borderBottomColor:'black',borderBottomWidth:0.4,zIndex:-100}}>
<View style={{width:width*45/100,height:50,borderRightColor:'black',borderRightWidth:0.5,justifyContent:'center'}}>
   <Text style={{marginLeft:'15%',color:'#2C2C2C'}}>End Time</Text>
     </View>
   
     <TouchableOpacity  onPress={()=>{ 
        showDatePicker()
        setpickerNo("endTime")
      }}

      style={{width:width*45/100,height:50}}>

{ pickerNo=="endTime" ? 
            <DateTimePickerModal
              value={endTime}
              isVisible={isDatePickerVisible}
              mode="time" 
              // locale='en_GB'
              is24Hour={false}
              timePickerModeAndroid="spinner"
              display="spinner"
              onConfirm={handleConfirmEndTime}
              onCancel={hideDatePicker}
            />
            :
            null
}

      <View style={{top:10}}>
         <Text style={{left:'7%'}}> {
          endTime.length > 0
          ? endTime
          : '__:__'
         
        }</Text>
                 
         </View>
    
     </TouchableOpacity>
 </View>
{/* *********************************Travel Time*********************************** */}
<View style={{width:width*90/100,flexDirection:'row',borderRadius:5,borderBottomColor:'black',borderBottomWidth:0.4,zIndex:-100}}>
<View style={{width:width*45/100,height:50,borderRightColor:'black',borderRightWidth:0.5,justifyContent:'center'}}>
   <Text style={{marginLeft:'15%',color:'#2C2C2C'}}>Travel Time</Text>
     </View>
     {
       jobTitle!='Warehouse' ?
     <View style={{width:width*45/100,height:50,zIndex:10000,}}>
   
     <Picker
       
        placeholder={"Select"}
        selectedValue={travelTime}
        style={{height: 40, width: '95%', left:'6%'}}
        onValueChange={(itemValue, itemIndex) =>{
            setTravelTime(itemValue)
            dateCalculate(jobTitle,date,myStartTime,enddate,myEndTime,itemValue)
        }
        }>
        {/* <Picker.Item label="Select" value="" /> */}
           {dropDownTravelTime.map((item, index) => {
                return <Picker.Item key={index} label={item.label} value={item.value} />
            })}
      </Picker> 
      {/* } */}

     </View>
:
 <View style={{width:width*45/100,height:50,zIndex:10000,justifyContent:'center'}}>
<Text style={{left:'9%'}}>NA</Text>
</View> 
    }
 </View>
{/* *****************************Total time**************************************** */}
<View style={{width:width*90/100,flexDirection:'row',borderRadius:5,borderBottomColor:'black',borderBottomWidth:0.4,zIndex:-10000}}>
<View style={{width:width*45/100,height:50,borderRightColor:'black',borderRightWidth:0.5,justifyContent:'center'}}>
   <Text style={{marginLeft:'15%',color:'#2C2C2C'}}>Total Time</Text>
     </View>

     <View  
      style={{width:width*45/100,height:50,top:5}}>

      <View style={{top:10,left:'5%'}}>

       
         <Text style={{left:9}}>{totalTime}</Text>
                 
         </View>
    
     </View>
 </View>
{/* ************************************************************************************ */}
<TouchableOpacity style={{width:width*90/100,height:50,justifyContent:'center',zIndex:-1000}} onPress={()=>{mySave()}}>
     <Text style={{alignSelf:'center',fontSize:16,color:'#2C2C2C'}}>Save</Text>
 </TouchableOpacity>

{/* ************************************************************************************ */}
</View>
   </View>
        
      <View style={{width:'95%',marginTop:'7%',zIndex:-1000}}>
        <Coustombutton bgColor={'white'} press={()=>{submit()}} textColor={'black'} title={'Add Another Job'}/>
      </View>  
     <View style={{width:100,height:100}} />  
       </KeyboardAwareScrollView>
       </ScrollView>
       </View>
       {
         loading  && (
          <View
          style={[
          StyleSheet.absoluteFill,
          { backgroundColor: 'rgba(0, 0, 0, 0.2)', justifyContent: 'center' }
          ]}
          >
              <ActivityIndicator color={"red"} size={40} />
      </View>
         )
       }

   </SafeAreaView>
    )
}

const styles = StyleSheet.create({

    container:{
       //flex:1,
        backgroundColor:"white",
        justifyContent:"space-between",
         alignItems:"center"
    },
    
  
 

})


export default Start