import React,{useState, useRef,useEffect} from 'react';
import {View , Text ,StyleSheet,Image,TouchableOpacity,Keyboard,StatusBar,TextInput,ActivityIndicator,ToastAndroid, Platform,Alert} from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

import AsyncStorage from '@react-native-community/async-storage';
import { useDispatch, useSelector } from 'react-redux';
import { SafeAreaView } from 'react-native-safe-area-context';
import Coustombutton from '../../Componant/Custombutton'
import { ScrollView } from 'react-native-gesture-handler';
import AwesomeAlert from 'react-native-awesome-alerts';
import Toast from 'react-native-simple-toast';
import * as types  from '../../redux/type';
import Axios from 'axios';
import ApiUrl from '../../ApiUrl/ApiUrl';
//import {submitLogin,saveUserResult} from/ '../../redux/actions/user_action';


const Login = (props) => {

    const [email,setEmail] = useState("");
    
    const [password,setPassword] = useState("");
    const [eye,setEye] = useState(true);

    const [isKeyboardVisible, setKeyboardVisible] = useState(false);

    const emailRef =  useRef();
    const passwordRef =  useRef();

    const dispatch = useDispatch();
    const loading = useSelector(state => state.common.loading);

    useEffect(() => {
      //  StatusBar.setHidden(true);
        const unsubscribe = props.navigation.addListener('focus', () => {
            setEmail("")
           
            setPassword("")
            const keyboardDidShowListener = Keyboard.addListener(
              'keyboardDidShow',
              () => {
                setKeyboardVisible(true); // or some other action
              } 
            );
            const keyboardDidHideListener = Keyboard.addListener(
              'keyboardDidHide',
              () => {
                setKeyboardVisible(false); // or some other action
              }
            );
        
            return () => {
              keyboardDidHideListener.remove();
              keyboardDidShowListener.remove();
            };
      
          });
        
          return () => {
            unsubscribe;
          };
    }, [props.navigation]);

const valide=()=>{
if(email==''){
    Toast.show('Please enter username');
    return false
}else if(email.trim() == ""){
    Toast.show('Please enter username');
    return false
}
else if(password==''){
  Toast.show('Please enter password');
  return false
}else{
    return true
}

}

    const setUserLogin = async() => {
    if(valide()){
      dispatch({
        type: types.LOADING,
        isLoading:true
    });
              let formdata = new FormData();
              formdata.append("user_name",email);
              formdata.append("password",password);
   
                 fetch(ApiUrl.base_url+ApiUrl.login, {
                 method: 'POST',
                 headers: {
                   'Content-Type': 'multipart/form-data',
                 },
                body: formdata
               }).then((response) => response.json())
                     .then((responseJson) => {

                      dispatch({
                        type: types.LOADING,
                        isLoading:false
                    });
           //   console.log('SignUp-->>',responseJson)
              if(responseJson.code==200){
            //    console.log("hi")
               AsyncStorage.setItem("user",JSON.stringify(responseJson));
              //  AsyncStorage.setItem("log",JSON.stringify(responseJson.code));
              // Toast.show(responseJson.message);   
                dispatch({
                    type:types.SAVE_USER_RESULTS,
                    user:responseJson
                }) 
                // AsyncStorage.setItem("log",responseJson.code);
            }else{
               Toast.show(responseJson.message);
            }
            
                     }).catch((error) => {
                        dispatch({
                            type: types.LOADING,
                            isLoading:false
                        });
                       
                     });
          }

    }

  

    return(
        <SafeAreaView style={{backgroundColor:'#FFB91D',flex:1}}>
     
      <StatusBar
          backgroundColor="#FFB91D"
          // barStyle="dark-content"
       />
      <ScrollView
      alwaysBounceVertical={false}>
       <KeyboardAwareScrollView 
       automaticallyAdjustContentInsets={ true }
       contentContainerStyle={{ backgroundColor:"#FFB91D",padding:10,flexGrow:1,alignItems:"center"}}
       >
               <Image source={require('../../Assets/logo.png')}  style={{height:200,width:200,marginTop:'20%'}}/>
 
                <Text style={{fontSize:20,fontWeight:'bold',marginTop:30,color:'#2C2C2C'}}>Log In</Text>
                <View style={{width:10,height:10,marginTop:'7%'}} />
                  <TextInput 
                    onChangeText={(value)=>{setEmail(value)}}
                    value={email}
                    placeholder="Username"
                  //  keyboardType={"number-pad"}
                   // placeholderTextColor="grey"
                   keyboardType={Platform.OS === 'ios' ? 'ascii-capable' : 'visible-password'}
                    style={styles.textInputStyle}
                    onSubmitEditing={()=> passwordRef.current.focus()}
                    />
               <View style={{width:'95%',alignSelf:'center'}}>
                  <TextInput 
                    ref={passwordRef}
                    onChangeText={(value)=>{setPassword(value)}}
                    value={password}
                    secureTextEntry={eye}
                    placeholder="Password"
                   // keyboardType={Platform.OS === 'ios' ? 'ascii-capable' : 'visible-password'}
                   // placeholderTextColor="grey"
                    style={{color:'black',backgroundColor:'white',marginVertical:15,borderRadius:5,paddingLeft:20,paddingRight:40,height:50}}
                    />
                 <TouchableOpacity onPress={()=>{setEye(!eye)}} style={{top:'35%',alignSelf:'flex-end',right:15,position:'absolute'}}>
                  <Image source={eye ? require('../../Assets/show_password.png'): require('../../Assets/hide_password.png')}style={{width:22,height:22}}></Image>
                  </TouchableOpacity>
              </View>
<View style={{width:'100%'}}>
<Text onPress={()=>{props.navigation.navigate('ForgotPassword')}} style={{alignSelf:'flex-end',right:20,top:10}}>Forgot Password ?</Text>
</View>
      <View style={{width:'95%',marginTop:'7%'}}>
        <Coustombutton bgColor={'black'} press={()=>{setUserLogin()}} textColor={'white'} title={'Log In'}/>
      </View>  

       </KeyboardAwareScrollView>
     
       </ScrollView>
       {/* <View style={{width:'70%',position:'absolute',bottom:10,alignSelf:'center'}}>
{Platform.OS=='android' ?
 !isKeyboardVisible ?
      <Text style={{alignSelf:'center',alignItems:'center'}}>Lorem Ipsum is simply dummy text of the printing and typesettin</Text>  
:
null
:
<Text style={{alignSelf:'center',alignItems:'center'}}>Lorem Ipsum is simply dummy text of the printing and typesettin</Text>
}
       </View> */}

       {
         loading  && (
          <View
          style={[
          StyleSheet.absoluteFill,
          { backgroundColor: 'rgba(0, 0, 0, 0.2)', justifyContent: 'center' }
          ]}
          >
              <ActivityIndicator color={"#FFB91D"} size={30} />
      </View>
         )
       }


        </SafeAreaView>
    )
}

const styles = StyleSheet.create({

    container:{
       //flex:1,
        backgroundColor:"white",
        justifyContent:"space-between",
         alignItems:"center"
    },
    
    textInputStyle:{
        width:"95%",
        color:'black',
        fontSize:16,
        paddingLeft:20,
       backgroundColor:'white',
       borderRadius:5,
       marginTop:15,
       height:50
    }

})


export default Login;