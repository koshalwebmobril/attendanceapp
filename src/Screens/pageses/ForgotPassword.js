import React,{useState, useRef,useEffect} from 'react';
import {View , Text,Dimensions ,StyleSheet,Image,TouchableOpacity,Keyboard,StatusBar,TextInput,ActivityIndicator,ToastAndroid, Platform,Alert} from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

import AsyncStorage from '@react-native-community/async-storage';
import { useDispatch, useSelector } from 'react-redux';
import { SafeAreaView } from 'react-native-safe-area-context';
import Coustombutton from '../../Componant/Custombutton'
import { ScrollView } from 'react-native-gesture-handler';
import AwesomeAlert from 'react-native-awesome-alerts';
import Toast from 'react-native-simple-toast';
import * as types  from '../../redux/type';
import Axios from 'axios';
import ApiUrl from '../../ApiUrl/ApiUrl';
const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;
//import {submitLogin,saveUserResult} from/ '../../redux/actions/user_action';


const ForgotPassword = (props) => {

    const [email,setEmail] = useState("");
    
    const [password,setPassword] = useState("");
    const [eye,setEye] = useState(true);

    const [isKeyboardVisible, setKeyboardVisible] = useState(false);

    const emailRef =  useRef();
    const passwordRef =  useRef();

    const dispatch = useDispatch();
    const loading = useSelector(state => state.common.loading);
    const user  = useSelector(state => state.user.user_details) 

    useEffect(() => {
        const unsubscribe = props.navigation.addListener('focus', () => {
            setEmail("")
           
            setPassword("")
            const keyboardDidShowListener = Keyboard.addListener(
              'keyboardDidShow',
              () => {
                setKeyboardVisible(true); // or some other action
              }
            );
            const keyboardDidHideListener = Keyboard.addListener(
              'keyboardDidHide',
              () => {
                setKeyboardVisible(false); // or some other action
              }
            );
        
            return () => {
              keyboardDidHideListener.remove();
              keyboardDidShowListener.remove();
            };
      
          });
        
          return () => {
            unsubscribe;
          };
    }, [props.navigation]);

const valide=()=>{
if(email==''){
    Toast.show('Please enter email');
    return false
}else if(!(/[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/.test(email))){

    Toast.show('Please enter valid email.');
          return false
    }else{
    return true
}

}

    const setUserLogin = async() => {
    if(valide()){
      dispatch({
        type: types.LOADING,
        isLoading:true
    });
              let formdata = new FormData();
              formdata.append("email",email);
            //   formdata.append("password",password);
   
                 fetch(ApiUrl.base_url+ApiUrl.forgot_password, {
                 method: 'POST',
                 headers: {
                   'Content-Type': 'multipart/form-data',
                   'Accept':'application/json',
                //    'Authorization': 'Bearer '+ user.data.token

                 },
                body: formdata
               }).then((response) => response.json())
                     .then((responseJson) => {
                     
            //  console.log('SignUp-->>',responseJson)
              if(responseJson.code==200){
                Toast.show(responseJson.message);
                dispatch({
                    type: types.LOADING,
                    isLoading:false
                });
            }else{
               Toast.show(responseJson.message);
               dispatch({
                type: types.LOADING,
                isLoading:false
            });
            }
            
                     }).catch((error) => {
                        dispatch({
                            type: types.LOADING,
                            isLoading:false
                        });
                       
                     });
          }

    }

  

    return(
        <SafeAreaView style={{backgroundColor:'rgba(0,0,0,0.8)',flex:1}}>
      <StatusBar backgroundColor='rgba(0,0,0,0.8)'/>
      {/* ************************Header************************** */}
      <View style={{width:'100%',height:50,backgroundColor:'#2C2C2C' ,flexDirection:'row',alignItems:'center',paddingLeft:20,paddingRight:20}}>
       <TouchableOpacity onPress={()=>{props.navigation.navigate('Login')}}style={{height:15,width:25}}>
           <Image source={require('../../Assets/v1.png')} style={{height:20,width:11}}></Image>
       </TouchableOpacity>
      
           <Text style={{color:'white',fontSize:17,fontWeight:'600',top:3}}>Back</Text>

       </View>

      {/* ************************end Header************************** */}
      <View style={{flex:1,width:windowWidth,height:windowHeight,backgroundColor:'#FFB91D'}}>
      <ScrollView>
       <KeyboardAwareScrollView 
       automaticallyAdjustContentInsets={ true }
       contentContainerStyle={{ backgroundColor:"#FFB91D",padding:10,flexGrow:1,alignItems:"center"}}
       >
               <Image source={require('../../Assets/logo.png')}  style={{height:200,width:200,marginTop:'20%'}}/>
 
                <Text style={{fontSize:20,fontWeight:'bold',marginTop:30,color:'#2C2C2C'}}>Forgot Password</Text>
                <View style={{width:10,height:10,marginTop:'7%'}} />
                  <TextInput 
                    onChangeText={(value)=>{setEmail(value)}}
                    value={email}
                    placeholder="Email"
                  //  keyboardType={"number-pad"}
                   // placeholderTextColor="grey"
                    style={styles.textInputStyle}
                    // onSubmitEditing={()=> passwordRef.current.focus()}
                    />
             
      <View style={{width:'95%',marginTop:'7%'}}>
        <Coustombutton bgColor={'black'} press={()=>{setUserLogin()}} textColor={'white'} title={'Submit'}/>
      </View>  

       </KeyboardAwareScrollView>
     
       </ScrollView>
       <View style={{width:'70%',position:'absolute',bottom:10,alignSelf:'center'}}>

       </View>
       </View>

       {
         loading  && (
          <View
          style={[
          StyleSheet.absoluteFill,
          { backgroundColor: 'rgba(0, 0, 0, 0.2)', justifyContent: 'center' }
          ]}
          >
              <ActivityIndicator color={"#FFB91D"} size={30} />
      </View>
         )
       }


        </SafeAreaView>
    )
}

const styles = StyleSheet.create({

    container:{
       //flex:1,
        backgroundColor:"white",
        justifyContent:"space-between",
         alignItems:"center"
    },
    
    textInputStyle:{
        width:"95%",
        color:'black',
        fontSize:16,
        paddingLeft:20,
       backgroundColor:'white',
       borderRadius:5,
       marginTop:15,
       height:50
    }

})


export default ForgotPassword;