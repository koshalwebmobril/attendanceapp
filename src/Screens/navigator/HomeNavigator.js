import React from  'react' ;
import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator, StackView } from '@react-navigation/stack'

import Home from '../pageses/Home';
import Select from '../pageses/Select';
import Start from '../pageses/Start';
import Mytimeseet from '../pageses/Mytimeseet';


const HomeStackNavigator = (props) => {

    const Stack = createStackNavigator();
    return(
        <NavigationContainer>
            <Stack.Navigator>
               
                    <Stack.Screen
                    component = {Home}
                    name="Home"
                    options={{
                        headerShown:false,
                        headerTitle:"Home",
                        headerTintColor:'white',
                        headerTitleAlign:'center',
                        headerTitleStyle:{
                            fontFamily:14,
                            fontFamily:'roboto-medium',
                        },
                        headerStyle: {
                           // height:40,
                            backgroundColor: '#2C684F',
                        },
                    }}
                
                />

                 <Stack.Screen
                    component = {Select}
                    name="Select"
                    options={{
                        headerShown:false,
                        headerTitle:"Select",
                        headerTintColor:'white',
                        headerTitleAlign:'center',
                        headerTitleStyle:{
                            fontFamily:14,
                            fontFamily:'roboto-medium',
                        },
                        headerStyle: {
                           // height:40,
                            backgroundColor: '#2C684F',
                        },
                    }}
                
                />
                 <Stack.Screen
                    component = {Start}
                    name="Start"
                    options={{
                        headerShown:false,
                        headerTitle:"Start",
                        headerTintColor:'white',
                        headerTitleAlign:'center',
                        headerTitleStyle:{
                            fontFamily:14,
                            fontFamily:'roboto-medium',
                        },
                        headerStyle: {
                           // height:40,
                            backgroundColor: '#2C684F',
                        },
                    }}
                
                />
              <Stack.Screen
                    component = {Mytimeseet}
                    name="Mytimeseet"
                    options={{
                        headerShown:false,
                        headerTitle:"Mytimeseet",
                        headerTintColor:'white',
                        headerTitleAlign:'center',
                        headerTitleStyle:{
                            fontFamily:14,
                            fontFamily:'roboto-medium',
                        },
                        headerStyle: {
                           // height:40,
                            backgroundColor: '#2C684F',
                        },
                    }}
                
                />
             


            </Stack.Navigator>
            </NavigationContainer>
    )
}

export default HomeStackNavigator;