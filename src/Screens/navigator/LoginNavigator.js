import 'react-native-gesture-handler';
import React, { useEffect, useState } from 'react';
import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator, StackView } from '@react-navigation/stack'
import Login from '../pageses/Login';
import ForgotPassword from '../pageses/ForgotPassword';
import {  useSelector, useDispatch } from 'react-redux';
const LoginNavigator =  (props) => {

   
    const Stack = createStackNavigator();
    return(
        <NavigationContainer>
            <Stack.Navigator options={{headerLayoutPreset: 'center'}}>   
               
                <Stack.Screen
                    component = {Login}
                    name="Login"
                    options={{
                        headerTitle:"Log In",
                        headerTintColor:'white',
                        headerShown:false,
                        headerTitleAlign:'center',
                        headerTitleStyle:{
                            fontFamily:14,
                            fontFamily:'roboto-medium'
                        },
                        headerStyle: {
                            backgroundColor: '#2C684F',
                        },
                    }}
                
                />               
                 <Stack.Screen
                    component = {ForgotPassword}
                    name="ForgotPassword"
                    options={{
                        headerTitle:"Forgot Password",
                        headerTintColor:'white',
                        headerShown:false,
                        headerTitleAlign:'center',
                        headerTitleStyle:{
                            fontFamily:14,
                            fontFamily:'roboto-medium'
                        },
                        headerStyle: {
                            backgroundColor: '#2C684F',
                        },
                    }}
                
                />             
            </Stack.Navigator>
        </NavigationContainer>

    )
}

export default LoginNavigator;