import React, { useEffect, useState } from 'react';
import LoginNavigator from './LoginNavigator';
import HomeNavigator from './HomeNavigator';
import Splash from '../Splash';
import SplashScreen from 'react-native-splash-screen'
import AsyncStorage from '@react-native-community/async-storage';
import {View ,StyleSheet,ActivityIndicator, Platform } from 'react-native';
import {  useSelector, useDispatch } from 'react-redux';
import {getAsyncStorage, setLoading} from '../../redux/actions/user_action';


  const AuthNavigator =  (props) => {
  
   
    const [showSplash , setShowSplash ]  = useState(true);
    
    const dispatch =  useDispatch();

    const isSignedIn  = useSelector(state => state.user.user_details) 

    useEffect(  ()=> {
  
        setTimeout(()=>{
           AsyncStorage.getItem("user").then(data =>{
            dispatch(getAsyncStorage(JSON.parse(data)))
            setShowSplash(false);
            SplashScreen.hide();
        })
        },Platform.OS=='android' ? 1000 : 2000)
        
        return () => {
                      //timeout;
                     }
      
    },[]);

if(showSplash){
   return (
      Platform.OS=="android" ? 
         null
             :
         <Splash/>
  
    )
}
    
    return (
        
            isSignedIn == null ?
                (
                    <LoginNavigator/>
                 //  <HomeNavigator />
                )
            :
                (
                  //  <LoginNavigator/>
                   <HomeNavigator />
                )
    )
}


export default AuthNavigator ;

