/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './App';
import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => App);

/**
 * ANDROID 
 * keystore password ---- andorid
 * alias - key0
 * alias pass- password
 */
