package com.attendance.com;

import android.os.Bundle;

import com.facebook.react.ReactActivity;
import org.devio.rn.splashscreen.SplashScreen; // here
// react-native-splash-screen < 0.3.1
//import com.cboy.rn.splashscreen.SplashScreen; // here
import android.os.Handler;
import android.view.View;

public class MainActivity extends ReactActivity {
//  private Handler mHandler;
  /**
   * Returns the name of the main component registered from JavaScript. This is used to schedule
   * rendering of the component.
   */

  @Override
  protected void onCreate(Bundle savedInstanceState) {
     SplashScreen.show(this);
    super.onCreate(savedInstanceState);
//    mHandler = new Handler();
  }

//  private void startProgress() {
//    new Thread(new Runnable() {
//      @Override
//
//      public void run() {
//        mHandler.postDelayed(new Runnable() {
//          @Override
//          public void run() {
//          getResources().getString(R.style.SplashTheme);
//           }
//        },5000);
//      }
//    });
//
//      }


  protected String getMainComponentName() {
    return "Attendance";
  }
}

